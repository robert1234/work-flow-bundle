<?php

namespace CieWorkFlowBundle\Controller\Api\Workflow;

use CieWorkFlowBundle\Service\WorkflowEntity\WorkflowQuerySrv;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

class WorkflowQueryController extends AbstractController
{
    /**
     * @Security("is_granted('ROLE_WORKFLOW_VIEW')")
     */
    public function getWorkflowsIndexAction(WorkflowQuerySrv $querySrv): JsonResponse
    {
        $content = $querySrv->getAll(true);

        return new JsonResponse($content);
    }

    /**
     * @Security("is_granted('ROLE_WORKFLOW_VIEW')")
     */
    public function getWorkflowAction($id, WorkflowQuerySrv $querySrv): JsonResponse
    {
        $content = $querySrv->getById($id, true);

        return new JsonResponse($content);
    }
}