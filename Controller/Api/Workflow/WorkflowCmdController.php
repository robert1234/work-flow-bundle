<?php

namespace CieWorkFlowBundle\Controller\Api\Workflow;

use CieWorkFlowBundle\Dto\AddWorkflowRequestDto;
use CieWorkFlowBundle\Dto\ArchiveWorkflowRequestDto;
use CieWorkFlowBundle\Dto\ArchiveWorkflowTransitionRequestDto;
use CieWorkFlowBundle\Dto\EditWorkflowRequestDto;
use CieWorkFlowBundle\Service\WorkflowEntity\AddWorkflowCommand;
use CieWorkFlowBundle\Service\WorkflowEntity\AddWorkflowWithTransitionsCommand;
use CieWorkFlowBundle\Service\WorkflowEntity\ArchiveWorkflowCommand;
use CieWorkFlowBundle\Service\WorkflowEntity\EditWorkflowCommand;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class WorkflowCmdController extends AbstractController
{
    /**
     * @Security("is_granted('ROLE_WORKFLOW_EDIT')")
     */
    public function addWorkflowAction(Request $request, AddWorkflowCommand $addWorkflowCommand): Response
    {
        $data = $request->request->all();
        $requestDto = AddWorkflowRequestDto::fromArray($data);

        $result = $addWorkflowCommand->execute($requestDto);

        return new JsonResponse($result->getId());
    }

    /**
     * @Security("is_granted('ROLE_WORKFLOW_EDIT')")
     */
    public function editWorkflowAction(Request $request, EditWorkflowCommand $editWorkflowCommand): Response
    {
        $data = $request->request->all();
        $requestDto = EditWorkflowRequestDto::fromArray($data);

        $result = $editWorkflowCommand->execute($requestDto);

        return new JsonResponse($result->getId());
    }

    /**
     * @Security("is_granted('ROLE_WORKFLOW_EDIT')")
     */
    public function archiveWorkflowAction(Request $request, ArchiveWorkflowCommand $archiveWorkflowCommand): Response
    {
        $data = $request->request->all();
        $requestDto = ArchiveWorkflowRequestDto::fromArray($data);

        $result = $archiveWorkflowCommand->execute($requestDto);

        return new JsonResponse($result->getId());
    }

    /**
     * @Security("is_granted('ROLE_WORKFLOW_EDIT')")
     */
    public function addWorkflowWithTransitionsAction(
        Request $request,
        AddWorkflowWithTransitionsCommand $addWorkflowWithTransitionsCommand
    ): Response
    {
        $data = $request->request->all();
        $requestDto = ArchiveWorkflowTransitionRequestDto::fromArray($data);

        $result = $addWorkflowWithTransitionsCommand->execute($requestDto);

        return new JsonResponse($result->getId());
    }
}