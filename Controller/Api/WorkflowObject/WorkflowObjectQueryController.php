<?php

namespace CieWorkFlowBundle\Controller\Api\WorkflowObject;

use CieWorkFlowBundle\Service\FlowableObjectRegistry;
use CieWorkFlowBundle\Service\PlaceActionRegistry;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

class WorkflowObjectQueryController extends AbstractController
{
    /**
     * @Security("is_granted('ROLE_WORKFLOW_VIEW')")
     */
    public function getWorkflowObjectIndexAction(FlowableObjectRegistry $registry): JsonResponse
    {
        $classList = $registry->getWorkflowObjects();

        return new JsonResponse($classList);
    }
}