<?php

namespace CieWorkFlowBundle\Controller\Api\WorkflowTransitionChecker;

use CieWorkFlowBundle\Service\WorkflowTransitionCheckerRegistry;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

class TransitionCheckerQuerySrv extends AbstractController
{
    /**
     * @Security("is_granted('ROLE_WORKFLOW_TRANSITION_VIEW')")
     */
    public function getCheckersIndexAction(WorkflowTransitionCheckerRegistry $registry): JsonResponse
    {
        $classList = $registry->getTransitionCheckerClassNames();

        return new JsonResponse($classList);
    }
}