<?php

namespace CieWorkFlowBundle\Controller\Api\PlaceAction;

use CieWorkFlowBundle\Service\PlaceActionRegistry;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

class PlaceActionQueryController extends AbstractController
{
    /**
     * @Security("is_granted('ROLE_WORKFLOW_STEP_VIEW')")
     */
    public function getPlaceActionsIndexAction(PlaceActionRegistry $registry): JsonResponse
    {
        $classList = $registry->getPlaceActionClassNames();

        return new JsonResponse($classList);
    }
}