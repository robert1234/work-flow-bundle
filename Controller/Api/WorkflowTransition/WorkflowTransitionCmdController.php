<?php

namespace CieWorkFlowBundle\Controller\Api\WorkflowTransition;

use CieWorkFlowBundle\Dto\AddWorkflowRequestDto;
use CieWorkFlowBundle\Dto\AddWorkflowTransitionRequestDto;
use CieWorkFlowBundle\Dto\ArchiveWorkflowTransitionRequestDto;
use CieWorkFlowBundle\Dto\EditWorkflowRequestDto;
use CieWorkFlowBundle\Dto\EditWorkflowTransitionRequestDto;
use CieWorkFlowBundle\Service\WorkflowTransitionEntity\AddTransitionCommand;
use CieWorkFlowBundle\Service\WorkflowTransitionEntity\ArchiveTransitionCommand;
use CieWorkFlowBundle\Service\WorkflowTransitionEntity\EditTransitionCommand;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class WorkflowTransitionCmdController extends AbstractController
{
    /**
     * @Security("is_granted('ROLE_WORKFLOW_TRANSITION_EDIT')")
     */
    public function addWorkflowTransitionAction(Request $request, AddTransitionCommand $addTransitionCommand): Response
    {
        $data = $request->request->all();
        $requestDto = AddWorkflowTransitionRequestDto::fromArray($data);

        $result = $addTransitionCommand->execute($requestDto);

        return new JsonResponse($result->getId());
    }

    /**
     * @Security("is_granted('ROLE_WORKFLOW_TRANSITION_EDIT')")
     */
    public function editWorkflowTransitionAction(Request $request, EditTransitionCommand $editTransitionCommand): Response
    {
        $data = $request->request->all();
        $requestDto = EditWorkflowTransitionRequestDto::fromArray($data);

        $result = $editTransitionCommand->execute($requestDto);

        return new JsonResponse($result->getId());
    }

    /**
     * @Security("is_granted('ROLE_WORKFLOW_TRANSITION_EDIT')")
     */
    public function archiveWorkflowTransitionAction(Request $request, ArchiveTransitionCommand $archiveTransitionCommand): Response
    {
        $data = $request->request->all();
        $requestDto = ArchiveWorkflowTransitionRequestDto::fromArray($data);

        $result = $archiveTransitionCommand->execute($requestDto);

        return new JsonResponse($result->getId());
    }
}