<?php

namespace CieWorkFlowBundle\Controller\Api\WorkflowTransition;

use CieWorkFlowBundle\Service\WorkflowTransitionEntity\WorkflowTransitionQuerySrv;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

class WorkflowTransitionQueryController extends AbstractController
{
    /**
     * @Security("is_granted('ROLE_WORKFLOW_TRANSITION_VIEW')")
     */
    public function getWorkflowTransitionsIndexAction(WorkflowTransitionQuerySrv $querySrv): JsonResponse
    {
        $content = $querySrv->getAll(true);

        return new JsonResponse($content);
    }

    /**
     * @Security("is_granted('ROLE_WORKFLOW_TRANSITION_VIEW')")
     */
    public function getWorkflowTransitionAction($id, WorkflowTransitionQuerySrv $querySrv): JsonResponse
    {
        $content = $querySrv->getById($id, true);

        return new JsonResponse($content);
    }
}