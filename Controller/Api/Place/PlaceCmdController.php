<?php

namespace CieWorkFlowBundle\Controller\Api\Place;

use CieWorkFlowBundle\Dto\AddPlaceRequestDto;
use CieWorkFlowBundle\Dto\ArchivePlaceRequestDto;
use CieWorkFlowBundle\Dto\EditPlaceRequestDto;
use CieWorkFlowBundle\Service\PlaceEntity\AddPlaceCommand;
use CieWorkFlowBundle\Service\PlaceEntity\ArchivePlaceCommand;
use CieWorkFlowBundle\Service\PlaceEntity\EditPlaceCommand;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PlaceCmdController extends AbstractController
{
    /**
     * @Security("is_granted('ROLE_WORKFLOW_STEP_EDIT')")
     */
    public function addPlaceAction(Request $request, AddPlaceCommand $addPlaceCommand): Response
    {
        $data = $request->request->all();
        $requestDto = AddPlaceRequestDto::fromArray($data);

        $result = $addPlaceCommand->execute($requestDto);

        return new JsonResponse($result->getId());
    }

    /**
     * @Security("is_granted('ROLE_WORKFLOW_STEP_EDIT')")
     */
    public function editPlaceAction(Request $request, EditPlaceCommand $editPlaceCommand): Response
    {
        $data = $request->request->all();
        $requestDto = EditPlaceRequestDto::fromArray($data);

        $result = $editPlaceCommand->execute($requestDto);

        return new JsonResponse($result->getId());
    }

    /**
     * @Security("is_granted('ROLE_WORKFLOW_STEP_EDIT')")
     */
    public function archivePlaceAction(Request $request, ArchivePlaceCommand $archivePlaceCommand): Response
    {
        $data = $request->request->all();
        $requestDto = ArchivePlaceRequestDto::fromArray($data);

        $result = $archivePlaceCommand->execute($requestDto);

        return new JsonResponse($result->getId());
    }
}