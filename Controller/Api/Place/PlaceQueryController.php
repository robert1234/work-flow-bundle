<?php

namespace CieWorkFlowBundle\Controller\Api\Place;

use CieWorkFlowBundle\Service\SerializerTool;
use CieWorkFlowBundle\Service\PlaceEntity\PlaceQuerySrv;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

class PlaceQueryController extends AbstractController
{
    /**
     * @Security("is_granted('ROLE_WORKFLOW_STEP_VIEW')")
     */
    public function getPlacesIndexAction(PlaceQuerySrv $placeQuerySrv): JsonResponse
    {
        $content = $placeQuerySrv->getAll(true);

        return new JsonResponse($content);
    }

    /**
     * @Security("is_granted('ROLE_WORKFLOW_STEP_VIEW')")
     */
    public function getPlaceAction($id, PlaceQuerySrv $placeQuerySrv): JsonResponse
    {
        $content = $placeQuerySrv->getById($id, true);

        return new JsonResponse($content);
    }

    /**
     * @Security("is_granted('ROLE_WORKFLOW_STEP_VIEW')")
     */
    public function getPlacesKeyPairListAction(PlaceQuerySrv $placeQuerySrv): JsonResponse
    {
        $data = $placeQuerySrv->getAllKeyPairList();
        $content = (new SerializerTool())->jsonSerialize($data);

        return new JsonResponse($content);
    }
}