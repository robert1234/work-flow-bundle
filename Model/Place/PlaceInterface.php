<?php

namespace CieWorkFlowBundle\Model\Place;

interface PlaceInterface
{
    public function getId(): int;
    public function getName(): string;
    public function getLabel(): ?string;
    public function getActionClass(): string;
}