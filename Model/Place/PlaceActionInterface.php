<?php

namespace CieWorkFlowBundle\Model\Place;

interface PlaceActionInterface
{
    public function handle($subject, $object, $transition = null);
    public function getName();
    public function getClassName(): string;
}