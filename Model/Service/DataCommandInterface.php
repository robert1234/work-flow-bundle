<?php

namespace CieWorkFlowBundle\Model\Service;

use CieWorkFlowBundle\Model\Dto\RequestDtoInterface;

interface DataCommandInterface
{
    public function execute(RequestDtoInterface $requestDto);
}