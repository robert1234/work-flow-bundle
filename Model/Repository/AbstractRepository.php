<?php

namespace CieWorkFlowBundle\Model\Repository;

use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

abstract class AbstractRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, $entityClass)
    {
        parent::__construct($registry, $entityClass);
    }

    /**
     * @param $object
     * @return mixed
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save($object)
    {
        $this->_em->persist($object);
        $this->_em->flush();
        return $object;
    }

    public function remove($object): void
    {
        $this->_em->remove($object);
        $this->_em->flush();
    }

    /** @param SoftRemovableTrait $object */
    public function archive($object)
    {
        $object->setArchivedAt(new DateTime());
        return $this->save($object);
    }
}