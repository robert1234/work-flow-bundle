<?php

namespace CieWorkFlowBundle\Model\Repository;

use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;

trait SoftRemovableTrait
{
    /**
     * @ORM\Column(name="archived_at", type="datetime", nullable=true)
     */
    protected ?DateTimeInterface $archivedAt;

    public function getArchivedAt(): ?DateTimeInterface
    {
        return $this->archivedAt;
    }

    public function setArchivedAt(?DateTimeInterface $archivedAt): self
    {
        $this->archivedAt = $archivedAt;
        return $this;
    }
}