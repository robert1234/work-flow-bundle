<?php

namespace CieWorkFlowBundle\Model\Manager;

use CieWorkFlowBundle\Model\Workflow\WorkflowInterface;
use CieWorkFlowBundle\Model\Workflow\WorkflowObjectInterface;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Workflow\Registry as WorkflowRegistry;

interface WorkflowManagerInterface
{
    public function buildWorkflow(WorkflowObjectInterface $object);
    public function getWorkflowRegistry(WorkflowObjectInterface $object): WorkflowRegistry;
    public function getInitialPoints(WorkflowInterface $workflow): iterable;
}