<?php

namespace CieWorkFlowBundle\Model\Manager;

use CieWorkFlowBundle\Model\Place\PlaceInterface;
use CieWorkFlowBundle\Model\Workflow\WorkflowObjectInterface;
use CieWorkFlowBundle\Model\Workflow\WorkflowTransitionInterface;
use Symfony\Component\Security\Core\User\UserInterface;

interface WorkflowTransitionPickerInterface
{
    public function can(
        WorkflowObjectInterface $workflowObject,
        UserInterface $user,
        WorkflowTransitionInterface $transition
    ): bool;

    public function do(
        WorkflowObjectInterface $workflowObject,
        UserInterface $user,
        WorkflowTransitionInterface $transition
    );

    public function availableTransitions(
        WorkflowObjectInterface $workflowObject,
        UserInterface $user,
        ?string $groupFilter = null
    ): iterable;
}