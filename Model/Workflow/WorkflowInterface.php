<?php

namespace CieWorkFlowBundle\Model\Workflow;

use CieWorkFlowBundle\Dto\AddWorkflowRequestDto;
use CieWorkFlowBundle\Model\Dto\RequestDtoInterface;

interface WorkflowInterface
{
    public function getSupportedClass(): string;
    public function isClassSupported($className): bool;
    public function getName(): string;
    public function getDescription(): ?string;
    public static function create(AddWorkflowRequestDto $requestDto);
}