<?php

namespace CieWorkFlowBundle\Model\Workflow;

use CieWorkFlowBundle\Model\Place\PlaceInterface;

interface WorkflowTransitionInterface
{
    public function getName(): string;
    public function getLabel(): ?string;
    public function getGroup(): ?string;
    public function getAdditionalAttributes(): array;
    public function getFrom(): PlaceInterface;
    public function getTo(): PlaceInterface;
    public function getWorkflow(): WorkflowInterface;
    public function getCheckerClass(): string;
}