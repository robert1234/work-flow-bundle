<?php

namespace CieWorkFlowBundle\Model\Workflow;

interface WorkflowTransitionCheckerInterface
{
    public function support($subject, $object, $transition = null): bool;
    public function getName();
    public function getClassName(): string;
}