<?php

namespace CieWorkFlowBundle\Model\Workflow;

use CieWorkFlowBundle\Entity\Workflow;
use CieWorkFlowBundle\Model\Place\PlaceInterface;

interface WorkflowObjectInterface
{
    /**
     * @return null|Workflow|WorkflowInterface
     */
    public function getWorkflow();
    public function getCurrentPlace(): string;
    public function setCurrentPlace(string $currentPlace);

    public function getClassName(): string;
    public function getHistoryClassName(): string;
    public function getStatusFieldName(): string;
}