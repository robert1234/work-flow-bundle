<?php

namespace CieWorkFlowBundle\Model\Workflow;

use CieWorkFlowBundle\Event\AuditEvent;
use Symfony\Component\Security\Core\User\UserInterface;

interface WorkflowAuditorInterface
{
    public function __invoke(AuditEvent $event);
}