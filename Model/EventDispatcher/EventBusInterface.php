<?php

namespace CieWorkFlowBundle\Model\EventDispatcher;

interface EventBusInterface
{
    public function handle($message);
}