<?php

namespace CieWorkFlowBundle\Model\Audit;

use CieWorkFlowBundle\Model\Workflow\WorkflowObjectInterface;
use DateTime;

interface WorkflowHistoryInterface
{
    /**
     * @return null|WorkflowObjectInterface
     */
    public function getFlowObject();
    public function setFlowObject(WorkflowObjectInterface $object): self;

    public function getLoggedPlace();
    public function setLoggedPlace($place): self;

    public function getLoggedTransition();
    public function setLoggedTransition($transition): self;

    public function getCreatedAt(): DateTime;
    public function setCreatedAt(DateTime $dateTime): self;

    public function getCreatedBy();
    public function setCreatedBy($user): self;
}