<?php

namespace CieWorkFlowBundle\Model\Dto;

interface RequestDtoInterface
{
    public static function fromArray(array $data): self;
}