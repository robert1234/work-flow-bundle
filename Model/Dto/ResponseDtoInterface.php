<?php

namespace CieWorkFlowBundle\Model\Dto;

interface ResponseDtoInterface
{
    public function toArray(): array;
}