<?php

namespace CieWorkFlowBundle\DependencyInjection;

use CieWorkFlowBundle\Model\Place\PlaceActionInterface;
use CieWorkFlowBundle\Model\Workflow\WorkflowTransitionCheckerInterface;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class CieWorkflowExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $this->processConfiguration($configuration, $configs);

        $loader = new YamlFileLoader(
            $container,
            new FileLocator([__DIR__.'/../'])
        );

        $loader->load('Resources/config/services.yml');

        $container->registerForAutoconfiguration(PlaceActionInterface::class)
            ->addTag('place.action.handler');

        $container->registerForAutoconfiguration(WorkflowTransitionCheckerInterface::class)
            ->addTag('workflow.transition.checker.handler');
    }
}