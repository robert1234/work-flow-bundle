<?php

namespace CieWorkFlowBundle\Service\WorkflowEntity;

use CieWorkFlowBundle\Dto\ArchiveWorkflowRequestDto;
use CieWorkFlowBundle\Entity\Workflow;
use CieWorkFlowBundle\Entity\WorkflowTransition;
use CieWorkFlowBundle\Model\Dto\RequestDtoInterface;
use CieWorkFlowBundle\Model\Service\DataCommandInterface;
use CieWorkFlowBundle\Repository\WorkflowRepository;
use CieWorkFlowBundle\Repository\WorkflowTransitionRepository;
use CieWorkFlowBundle\Validator\ValidatorService\ValidationSrv;
use DateTime;
use Doctrine\ORM\EntityNotFoundException;
use InvalidArgumentException;

class ArchiveWorkflowCommand implements DataCommandInterface
{
    private WorkflowRepository $workflowRepository;
    private WorkflowTransitionRepository $workflowTransitionRepository;
    private ValidationSrv $validationSrv;

    public function __construct(
        WorkflowRepository $workflowRepository,
        WorkflowTransitionRepository $workflowTransitionRepository,
        ValidationSrv $validationSrv
    )
    {
        $this->workflowRepository = $workflowRepository;
        $this->workflowTransitionRepository = $workflowTransitionRepository;
        $this->validationSrv = $validationSrv;
    }

    public function execute(RequestDtoInterface $requestDto): Workflow
    {
        if (! $requestDto instanceof ArchiveWorkflowRequestDto) {
            throw new InvalidArgumentException(sprintf("Only %s is valid!", ArchiveWorkflowRequestDto::class));
        }
        $this->validationSrv->validateAndThrow($requestDto);

        /** @var Workflow $workflow */
        $workflow = $this->workflowRepository->getObjectById($requestDto->id);

        if (! $workflow) {
            throw new EntityNotFoundException(sprintf("Entity workflow#%s not exists!", $requestDto->id));
        }

        $this->archiveTransitions($workflow->getTransitions());
        $this->workflowRepository->archive($workflow);

        return $workflow;
    }

    public function archiveTransitions(iterable $transitions)
    {
        /** @var WorkflowTransition $transition */
        foreach ($transitions as $transition) {
            $transition->setArchivedAt(new DateTime());
            $this->workflowTransitionRepository->archive($transition);
        }
    }
}