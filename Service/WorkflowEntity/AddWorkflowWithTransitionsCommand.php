<?php

namespace CieWorkFlowBundle\Service\WorkflowEntity;

use CieWorkFlowBundle\Dto\AddWorkflowWithTransitionsRequestDto;
use CieWorkFlowBundle\Entity\Workflow;
use CieWorkFlowBundle\Model\Dto\RequestDtoInterface;
use CieWorkFlowBundle\Model\Service\DataCommandInterface;
use CieWorkFlowBundle\Repository\WorkflowRepository;
use CieWorkFlowBundle\Repository\WorkflowTransitionRepository;
use CieWorkFlowBundle\Service\WorkflowTransitionEntity\WorkflowTransitionFactory;
use CieWorkFlowBundle\Validator\ValidatorService\ValidationSrv;
use InvalidArgumentException;

class AddWorkflowWithTransitionsCommand implements DataCommandInterface
{
    private WorkflowRepository $workflowRepository;
    private WorkflowTransitionRepository $workflowTransitionRepository;
    private WorkflowTransitionFactory $factory;
    private ValidationSrv $validationSrv;

    public function __construct(
        WorkflowRepository $workflowRepository,
        WorkflowTransitionRepository $workflowTransitionRepository,
        WorkflowTransitionFactory $factory,
        ValidationSrv $validationSrv
    )
    {
        $this->workflowRepository = $workflowRepository;
        $this->workflowTransitionRepository = $workflowTransitionRepository;
        $this->factory = $factory;
        $this->validationSrv = $validationSrv;
    }

    public function execute(RequestDtoInterface $requestDto): Workflow
    {
        if (! $requestDto instanceof AddWorkflowWithTransitionsRequestDto) {
            throw new InvalidArgumentException(sprintf(
                "Only %s is valid!",
                AddWorkflowWithTransitionsRequestDto::class
            ));
        }

        $this->validationSrv->validateAndThrow($requestDto->workflowRequestDto);

        $workflow = Workflow::create($requestDto->workflowRequestDto);
        $workflow = $this->workflowRepository->save($workflow);

        foreach ($requestDto->workflowTransitionRequestDtos as $workflowTransitionRequestDto) {
            $this->validationSrv->validateAndThrow($workflowTransitionRequestDto);

            $transition = $this->factory->fromRequest($workflowTransitionRequestDto);
            $this->workflowTransitionRepository->save($transition);
        }

        return $workflow;
    }
}