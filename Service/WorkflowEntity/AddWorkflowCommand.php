<?php

namespace CieWorkFlowBundle\Service\WorkflowEntity;

use CieWorkFlowBundle\Dto\AddWorkflowRequestDto;
use CieWorkFlowBundle\Entity\Workflow;
use CieWorkFlowBundle\Model\Dto\RequestDtoInterface;
use CieWorkFlowBundle\Model\Service\DataCommandInterface;
use CieWorkFlowBundle\Repository\WorkflowRepository;
use CieWorkFlowBundle\Validator\ValidatorService\ValidationSrv;
use InvalidArgumentException;

class AddWorkflowCommand implements DataCommandInterface
{
    private WorkflowRepository $workflowRepository;
    private ValidationSrv $validationSrv;

    public function __construct(WorkflowRepository $workflowRepository, ValidationSrv $validationSrv)
    {
        $this->workflowRepository = $workflowRepository;
        $this->validationSrv = $validationSrv;
    }

    public function execute(RequestDtoInterface $requestDto): Workflow
    {
        if (! $requestDto instanceof AddWorkflowRequestDto) {
            throw new InvalidArgumentException(sprintf("Only %s is valid!", AddWorkflowRequestDto::class));
        }

        $this->validationSrv->validateAndThrow($requestDto);

        $workflow = Workflow::create($requestDto);
        return $this->workflowRepository->save($workflow);
    }
}