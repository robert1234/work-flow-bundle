<?php

namespace CieWorkFlowBundle\Service\WorkflowEntity;

use CieWorkFlowBundle\Entity\Workflow;
use CieWorkFlowBundle\Model\Workflow\WorkflowInterface;
use CieWorkFlowBundle\Repository\WorkflowRepository;
use CieWorkFlowBundle\Repository\WorkflowTransitionRepository;
use CieWorkFlowBundle\Service\SerializerTool;

class WorkflowQuerySrv
{
    private WorkflowRepository $workflowRepository;
    private WorkflowTransitionRepository $workflowTransitionRepository;

    public function __construct(WorkflowRepository $workflowRepository, WorkflowTransitionRepository $workflowTransitionRepository)
    {
        $this->workflowRepository = $workflowRepository;
        $this->workflowTransitionRepository = $workflowTransitionRepository;
    }

    public function getAll(bool $serialized = false)
    {
        $workflows = $this->workflowRepository->findAllUnarchived();

        if ($serialized) {
            $workflows = array_values(array_map(function (Workflow $workflow) {
                return $workflow->toArray();
            }, $workflows));
        }

        return $workflows;
    }

    public function getById($id, bool $serialized = false)
    {
        $step = $this->workflowRepository->getObjectById($id);

        if ($serialized) {
            $serializer = new SerializerTool();
            return $serializer->toArray($step);
        }

        return $step;
    }

    public function getByName(string $name): ?WorkflowInterface
    {
        return $this->workflowRepository->getObjectByName($name);
    }
}