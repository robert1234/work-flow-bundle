<?php

namespace CieWorkFlowBundle\Service\WorkflowEntity;

use CieWorkFlowBundle\Dto\EditWorkflowRequestDto;
use CieWorkFlowBundle\Entity\Workflow;
use CieWorkFlowBundle\Model\Dto\RequestDtoInterface;
use CieWorkFlowBundle\Model\Service\DataCommandInterface;
use CieWorkFlowBundle\Repository\WorkflowRepository;
use CieWorkFlowBundle\Validator\ValidatorService\ValidationSrv;
use Doctrine\ORM\EntityNotFoundException;
use InvalidArgumentException;

class EditWorkflowCommand implements DataCommandInterface
{
    private WorkflowRepository $workflowRepository;
    private ValidationSrv $validationSrv;

    public function __construct(WorkflowRepository $workflowRepository, ValidationSrv $validationSrv)
    {
        $this->workflowRepository = $workflowRepository;
        $this->validationSrv = $validationSrv;
    }

    public function execute(RequestDtoInterface $requestDto)
    {
        if (! $requestDto instanceof EditWorkflowRequestDto) {
            throw new InvalidArgumentException(sprintf("Only %s is valid!", EditWorkflowRequestDto::class));
        }
        $this->validationSrv->validateAndThrow($requestDto);

        /** @var Workflow $workflow */
        $workflow = $this->workflowRepository->getObjectById($requestDto->id);

        if (! $workflow) {
            throw new EntityNotFoundException(sprintf("Entity workflow#%s not exists!", $requestDto->id));
        }

        $workflow->setName($requestDto->name);
        $workflow->setSupportedClass($requestDto->supportedClass);
        $workflow->setDescription($requestDto->description);

        $workflow->setAdditionalAttributes($requestDto->additionalAttributes);

        return $this->workflowRepository->save($workflow);
    }
}