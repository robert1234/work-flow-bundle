<?php

namespace CieWorkFlowBundle\Service;

use CieWorkFlowBundle\Model\Workflow\WorkflowTransitionCheckerInterface;
use Doctrine\Common\Collections\ArrayCollection;
use InvalidArgumentException;

class WorkflowTransitionCheckerRegistry
{
    /** @var WorkflowTransitionCheckerInterface[] */
    private iterable $handlers;

    public function __construct($transitionCheckers)
    {
        $this->handlers = $transitionCheckers;
    }

    /**
     * @return WorkflowTransitionCheckerInterface[]
     */
    public function getHandlers()
    {
        return $this->handlers;
    }

    public function getTransitionChecker($checkerClassName): WorkflowTransitionCheckerInterface
    {
        foreach ($this->handlers as $handler) {
            if ($handler->getName() == $checkerClassName) {
                return $handler;
            }
        }

        throw new InvalidArgumentException(
            sprintf("Invalid step action class name: %s!", $checkerClassName)
        );
    }

    public function getTransitionCheckerClassNames(): array
    {
        $result = new ArrayCollection();
        foreach ($this->handlers as $handler) {
            $result->add($handler->getName());
        }

        return $result->toArray();
    }
}