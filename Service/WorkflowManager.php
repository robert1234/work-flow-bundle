<?php

namespace CieWorkFlowBundle\Service;

use CieWorkFlowBundle\Dto\TransitionInfoDto;
use CieWorkFlowBundle\Model\Manager\WorkflowManagerInterface;
use CieWorkFlowBundle\Model\Place\PlaceInterface;
use CieWorkFlowBundle\Model\Workflow\WorkflowInterface;
use CieWorkFlowBundle\Model\Workflow\WorkflowObjectInterface;
use CieWorkFlowBundle\Repository\PlaceRepository;
use CieWorkFlowBundle\Repository\WorkflowRepository;
use CieWorkFlowBundle\Repository\WorkflowTransitionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Workflow\DefinitionBuilder;
use Symfony\Component\Workflow\MarkingStore\MethodMarkingStore;
use Symfony\Component\Workflow\Registry;
use Symfony\Component\Workflow\Registry as WorkflowRegistry;
use Symfony\Component\Workflow\SupportStrategy\InstanceOfSupportStrategy;
use Symfony\Component\Workflow\Workflow;

class WorkflowManager implements WorkflowManagerInterface
{
    private WorkflowTransitionRepository $workflowTransitionRepository;
    private PlaceRepository $stepRepository;
    private WorkflowRepository $workflowRepository;

    public function __construct(
        WorkflowTransitionRepository $workflowTransitionRepository,
        PlaceRepository $stepRepository,
        WorkflowRepository $workflowRepository
    ) {
        $this->workflowTransitionRepository = $workflowTransitionRepository;
        $this->stepRepository = $stepRepository;
        $this->workflowRepository = $workflowRepository;
    }

    public function buildWorkflow(WorkflowObjectInterface $object): Workflow
    {
        $definitionBuilder = new DefinitionBuilder();

        $workflow = $object->getWorkflow();
        $steps = $this->stepRepository->findByWorkflow($workflow);
        $transitions = $this->workflowTransitionRepository->findByWorkflow($workflow);

        $singleState = true;
        $property = $object->getStatusFieldName();
        $markingStore = new MethodMarkingStore($singleState, $property);

        /** @var PlaceInterface $step */
        foreach ($steps as $step) {
            $definitionBuilder->addPlace($step->getName());
        }

        foreach ($transitions as $transition) {
            $transitionDto = TransitionInfoDto::create($transition);
            $definitionBuilder->addTransition($transitionDto->toSymfonyTransition());
        }

        $definition = $definitionBuilder->build();

        return new Workflow($definition, $markingStore);
    }

    public function getWorkflowRegistry(WorkflowObjectInterface $object): WorkflowRegistry
    {
        $registry = new Registry();
        $workflow = $this->buildWorkflow($object);
        $instanceOfSupportStrategy = new InstanceOfSupportStrategy($object->getClassName());

        $registry->addWorkflow($workflow, $instanceOfSupportStrategy);
        return $registry;
    }

    public function getInitialPoints(WorkflowInterface $workflow): iterable
    {
        $fromPointNames = array_column($this->stepRepository->findNamesByWorkflowAtFrom($workflow), 'name');
        $toPointNames = array_column($this->stepRepository->findNamesByWorkflowAtTo($workflow), 'name');

        $availableStartingPoints = array_diff($fromPointNames, $toPointNames);

        $result = new ArrayCollection();
        foreach ($availableStartingPoints as $startingPoint) {
            $step = $this->stepRepository->getObjectByName($startingPoint);
            $result->set($startingPoint, $step);
        }

        return $result;
    }
}