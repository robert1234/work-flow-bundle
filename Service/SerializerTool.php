<?php

namespace CieWorkFlowBundle\Service;

use JMS\Serializer\SerializerBuilder;

class SerializerTool
{
    private $serializer;

    public function __construct()
    {
        $this->serializer = SerializerBuilder::create()->build();
    }

    public function jsonSerialize($data)
    {
        return $this->serializer->serialize($data, 'json');
    }

    public function jsonDeserialize($content, $type)
    {
        return $this->serializer->deserialize($content, $type, 'json');
    }

    public function toArray($data)
    {
        return json_decode($this->jsonSerialize($data));
    }

    public function fromArray($content, $type)
    {
        return $this->jsonDeserialize(json_encode($content), $type);
    }
}