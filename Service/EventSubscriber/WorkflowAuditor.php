<?php

namespace CieWorkFlowBundle\Service\EventSubscriber;

use CieWorkFlowBundle\Event\AuditEvent;
use CieWorkFlowBundle\Model\Audit\WorkflowHistoryInterface;
use CieWorkFlowBundle\Model\Workflow\WorkflowAuditorInterface;
use CieWorkFlowBundle\Repository\WorkflowRepository;
use DateTime;

class WorkflowAuditor implements WorkflowAuditorInterface
{
    private WorkflowRepository $workflowRepository;

    public function __construct(WorkflowRepository $workflowRepository)
    {
        $this->workflowRepository = $workflowRepository;
    }

    public function __invoke(AuditEvent $event)
    {
        $this->log($event);
    }

    public function log(AuditEvent $event)
    {
        $historyEntityClass = $event->getWorkflowObject()->getHistoryClassName();

        /** @var WorkflowHistoryInterface $historyEntity */
        $historyEntity = new $historyEntityClass();

        $historyEntity->setFlowObject($event->getWorkflowObject());
        $historyEntity->setLoggedTransition($event->getTransition()->getName());
        $historyEntity->setLoggedPlace($event->getTransition()->getTo()->getName());
        $historyEntity->setCreatedBy($event->getUser());
        $historyEntity->setCreatedAt(new DateTime());

        $this->workflowRepository->save($historyEntity);
    }
}