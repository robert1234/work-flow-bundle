<?php

namespace CieWorkFlowBundle\Service;

use CieWorkFlowBundle\Model\Workflow\WorkflowObjectInterface;
use CieWorkFlowBundle\Model\Workflow\WorkflowTransitionInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Workflow\Workflow;

class WorkflowApplier
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function apply(
        WorkflowObjectInterface $workflowObject, Workflow $symfonyWorkflow, WorkflowTransitionInterface $transition
    ): WorkflowObjectInterface
    {
        $symfonyWorkflow->apply($workflowObject, $transition->getName());
        $this->entityManager->persist($workflowObject);
        $this->entityManager->flush();

        return $workflowObject;
    }
}