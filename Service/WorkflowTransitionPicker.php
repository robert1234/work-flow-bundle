<?php

namespace CieWorkFlowBundle\Service;

use CieWorkFlowBundle\Event\AuditEvent;
use CieWorkFlowBundle\EventDispatcher\WorkflowEventBus;
use CieWorkFlowBundle\Model\Manager\WorkflowManagerInterface;
use CieWorkFlowBundle\Model\Manager\WorkflowTransitionPickerInterface;
use CieWorkFlowBundle\Model\Workflow\WorkflowObjectInterface;
use CieWorkFlowBundle\Model\Workflow\WorkflowTransitionInterface;
use CieWorkFlowBundle\Repository\WorkflowTransitionRepository;
use CieWorkFlowBundle\Service\EventSubscriber\WorkflowAuditor;
use Symfony\Component\Security\Core\User\UserInterface;

class WorkflowTransitionPicker implements WorkflowTransitionPickerInterface
{
    private WorkflowManagerInterface $workflowManager;
    private WorkflowApplier $workflowApplier;
    private WorkflowAuditor $workflowAuditor;
    private WorkflowTransitionRepository $workflowTransitionRepository;
    private WorkflowEventBus $eventBus;
    private PlaceActionRegistry $stepActionRegistry;
    private WorkflowTransitionCheckerRegistry $transitionCheckerRegistry;

    public function __construct(
        WorkflowManagerInterface $workflowManager,
        WorkflowApplier $workflowApplier,
        WorkflowTransitionRepository $workflowTransitionRepository,
        WorkflowEventBus $eventBus,
        PlaceActionRegistry $stepActionRegistry,
        WorkflowAuditor $workflowAuditor,
        WorkflowTransitionCheckerRegistry $transitionCheckerRegistry
    )
    {
        $this->workflowManager = $workflowManager;
        $this->workflowApplier = $workflowApplier;
        $this->workflowTransitionRepository = $workflowTransitionRepository;
        $this->eventBus = $eventBus;
        $this->stepActionRegistry = $stepActionRegistry;
        $this->workflowAuditor = $workflowAuditor;
        $this->transitionCheckerRegistry = $transitionCheckerRegistry;
    }

    public function can(WorkflowObjectInterface $workflowObject, UserInterface $user, WorkflowTransitionInterface $transition): bool
    {
        $registry = $this->workflowManager->getWorkflowRegistry($workflowObject);
        $workflow = $registry->get($workflowObject);

        $can = $workflow->can($workflowObject, $transition->getName());

        $checkerClass = $transition->getCheckerClass();
        $checkerClassInstance = $this->transitionCheckerRegistry->getTransitionChecker($checkerClass);

        $supports = $checkerClassInstance->support($user, $workflowObject, $transition);

        return $can && $supports;
    }

    public function do(WorkflowObjectInterface $workflowObject, UserInterface $user, WorkflowTransitionInterface $transition)
    {
        $registry = $this->workflowManager->getWorkflowRegistry($workflowObject);
        $workflow = $registry->get($workflowObject);
        $stepActionClass = $transition->getTo()->getActionClass();

        $stepActionInstance = $this->stepActionRegistry->getPlaceAction($stepActionClass);

        $workflowObject = $this->workflowApplier->apply($workflowObject, $workflow, $transition);
        $this->workflowAuditor->log(AuditEvent::create($workflowObject, $transition, $user));

        $stepActionInstance->handle($user, $workflowObject, $transition);

        return $workflowObject;
    }

    public function availableTransitions(
        WorkflowObjectInterface $workflowObject,
        UserInterface $user,
        ?string $groupFilter = null
    ): iterable
    {
        $registry = $this->workflowManager->getWorkflowRegistry($workflowObject);
        $workflow = $registry->get($workflowObject);

        $symfonyTransitions = $workflow->getEnabledTransitions($workflowObject);
        $transitions = $this->workflowTransitionRepository->getObjectsBySymfonyTransitions($symfonyTransitions);

        $filtered = $transitions->filter(function (WorkflowTransitionInterface $transition) use ($workflowObject, $user) {
            $checkerClass = $transition->getCheckerClass();
            $checkerClassInstance = $this->transitionCheckerRegistry->getTransitionChecker($checkerClass);
            return $checkerClassInstance->support($user, $workflowObject, $transition);
        });

        if ($groupFilter) {
            $filtered = $transitions->filter(function (WorkflowTransitionInterface $transition) use ($groupFilter) {
                return $transition->getGroup() == $groupFilter;
            });
        }

        return $filtered;
    }
}