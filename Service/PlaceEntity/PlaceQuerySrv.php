<?php

namespace CieWorkFlowBundle\Service\PlaceEntity;

use CieWorkFlowBundle\Entity\Place;
use CieWorkFlowBundle\Model\Place\PlaceInterface;
use CieWorkFlowBundle\Repository\PlaceRepository;
use CieWorkFlowBundle\Service\SerializerTool;

class PlaceQuerySrv
{
    private PlaceRepository $stepRepository;

    public function __construct(PlaceRepository $stepRepository)
    {
        $this->stepRepository = $stepRepository;
    }

    public function getAll(bool $serialized = false)
    {
        $steps = $this->stepRepository->findAllUnarchived();

        if ($serialized) {
            return array_values(array_map(function (Place $step) {
                return $step->toArray();
            }, $steps));
        }

        return $steps;
    }

    public function getById($id, bool $serialized = false)
    {
        $step = $this->stepRepository->getObjectById($id);

        if ($serialized) {
            return $step->toArray();
        }

        return $step;
    }

    public function getByName(string $name): ?PlaceInterface
    {
        return $this->stepRepository->getObjectByName($name);
    }

    public function getAllKeyPairList()
    {
        $steps = $this->stepRepository->findAllUnarchived();

        $result = [];

        foreach ($steps as $step) {
            $result[ $step->getId() ] = $step->getLabel() ? $step->getLabel() : $step->getName();
        }
        return $result;
    }
}