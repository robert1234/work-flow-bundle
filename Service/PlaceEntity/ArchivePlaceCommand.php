<?php

namespace CieWorkFlowBundle\Service\PlaceEntity;

use CieWorkFlowBundle\Dto\ArchivePlaceRequestDto;
use CieWorkFlowBundle\Entity\Place;
use CieWorkFlowBundle\Model\Dto\RequestDtoInterface;
use CieWorkFlowBundle\Model\Service\DataCommandInterface;
use CieWorkFlowBundle\Model\Place\PlaceInterface;
use CieWorkFlowBundle\Repository\PlaceRepository;
use CieWorkFlowBundle\Validator\ValidatorService\ValidationSrv;
use Doctrine\ORM\EntityNotFoundException;
use InvalidArgumentException;

class ArchivePlaceCommand implements DataCommandInterface
{
    private PlaceRepository $stepRepository;
    private ValidationSrv $validationSrv;

    public function __construct(PlaceRepository $stepRepository, ValidationSrv $validationSrv)
    {
        $this->stepRepository = $stepRepository;
        $this->validationSrv = $validationSrv;
    }

    public function execute(RequestDtoInterface $requestDto): PlaceInterface
    {
        if (! $requestDto instanceof ArchivePlaceRequestDto) {
            throw new InvalidArgumentException(sprintf("Only %s is valid!", ArchivePlaceRequestDto::class));
        }

        $this->validationSrv->validateAndThrow($requestDto);

        /** @var Place $step */
        $step = $this->stepRepository->getObjectById($requestDto->id);

        if (! $step) {
            throw new EntityNotFoundException(sprintf("Entity step#%s not exists!", $requestDto->id));
        }

        return $this->stepRepository->archive($step);
    }
}