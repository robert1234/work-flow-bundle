<?php

namespace CieWorkFlowBundle\Service\PlaceEntity;

use CieWorkFlowBundle\Dto\EditPlaceRequestDto;
use CieWorkFlowBundle\Entity\Place;
use CieWorkFlowBundle\Model\Dto\RequestDtoInterface;
use CieWorkFlowBundle\Model\Place\PlaceInterface;
use CieWorkFlowBundle\Repository\PlaceRepository;
use CieWorkFlowBundle\Validator\ValidatorService\ValidationSrv;
use Doctrine\ORM\EntityNotFoundException;
use InvalidArgumentException;

class EditPlaceCommand
{
    private PlaceRepository $stepRepository;
    private ValidationSrv $validationSrv;

    public function __construct(PlaceRepository $stepRepository, ValidationSrv $validationSrv)
    {
        $this->stepRepository = $stepRepository;
        $this->validationSrv = $validationSrv;
    }

    public function execute(RequestDtoInterface $requestDto): PlaceInterface
    {
        if (! $requestDto instanceof EditPlaceRequestDto) {
            throw new InvalidArgumentException(sprintf("Only %s is valid!", EditPlaceRequestDto::class));
        }

        $this->validationSrv->validateAndThrow($requestDto);

        /** @var Place $step */
        $step = $this->stepRepository->getObjectById($requestDto->id);

        if (! $step) {
            throw new EntityNotFoundException(sprintf("Entity step#%s not exists!", $requestDto->id));
        }

        $step->setName($requestDto->name);
        $step->setActionClass($requestDto->actionClass);
        $step->setAdditionalAttributes($requestDto->additionalAttributes);
        $step->setLabel($requestDto->label);

        return $this->stepRepository->save($step);
    }
}