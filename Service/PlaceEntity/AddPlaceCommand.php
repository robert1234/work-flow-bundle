<?php

namespace CieWorkFlowBundle\Service\PlaceEntity;

use CieWorkFlowBundle\Dto\AddPlaceRequestDto;
use CieWorkFlowBundle\Entity\Place;
use CieWorkFlowBundle\Model\Dto\RequestDtoInterface;
use CieWorkFlowBundle\Model\Service\DataCommandInterface;
use CieWorkFlowBundle\Model\Place\PlaceInterface;
use CieWorkFlowBundle\Repository\PlaceRepository;
use CieWorkFlowBundle\Validator\ValidatorService\ValidationSrv;
use InvalidArgumentException;

class AddPlaceCommand implements DataCommandInterface
{
    private PlaceRepository $stepRepository;
    private ValidationSrv $validationSrv;

    public function __construct(PlaceRepository $stepRepository, ValidationSrv $validationSrv)
    {
        $this->stepRepository = $stepRepository;
        $this->validationSrv = $validationSrv;
    }

    public function execute(RequestDtoInterface $requestDto): PlaceInterface
    {
        if (! $requestDto instanceof AddPlaceRequestDto) {
            throw new InvalidArgumentException(sprintf("Only %s is valid!", AddPlaceRequestDto::class));
        }

        $this->validationSrv->validateAndThrow($requestDto);

        $step = Place::create($requestDto);
        return $this->stepRepository->save($step);
    }
}