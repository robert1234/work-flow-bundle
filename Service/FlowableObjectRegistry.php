<?php

namespace CieWorkFlowBundle\Service;

use CieWorkFlowBundle\Model\Workflow\WorkflowObjectInterface;
use Doctrine\Common\Collections\ArrayCollection;
use ReflectionClass;
use Throwable;

class FlowableObjectRegistry
{
    public function getWorkflowObjects(): array
    {
        $classList = get_declared_classes();
        $result = new ArrayCollection();
        foreach($classList as $class) {
            try {
                $reflect = new ReflectionClass($class);
                if ($reflect->implementsInterface(WorkflowObjectInterface::class)) {
                    $result->add($class);
                }
            } catch (Throwable $throwable) {
                continue;
            }
        }

        return $result->toArray();
    }
}