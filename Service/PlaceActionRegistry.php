<?php

namespace CieWorkFlowBundle\Service;

use CieWorkFlowBundle\Model\Place\PlaceActionInterface;
use Doctrine\Common\Collections\ArrayCollection;
use InvalidArgumentException;

class PlaceActionRegistry
{
    /** @var PlaceActionInterface[] */
    private iterable $placeActions;

    public function __construct($placeActions)
    {
        $this->placeActions = $placeActions;
    }

    /**
     * @return PlaceActionInterface[]
     */
    public function getStepActions(): iterable
    {
        return $this->placeActions;
    }

    public function getPlaceAction($placeActionClassName): PlaceActionInterface
    {
        foreach ($this->placeActions as $placeAction) {
            if ($placeAction->getName() == $placeActionClassName) {
                return $placeAction;
            }
        }

        throw new InvalidArgumentException(
            sprintf("Invalid step action class name: %s!", $placeActionClassName)
        );
    }

    public function getPlaceActionClassNames(): array
    {
        $result = new ArrayCollection();
        foreach ($this->placeActions as $placeAction) {
            $result->add($placeAction->getName());
        }

        return $result->toArray();
    }
}