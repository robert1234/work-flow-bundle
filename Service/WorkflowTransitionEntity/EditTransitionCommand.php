<?php

namespace CieWorkFlowBundle\Service\WorkflowTransitionEntity;

use CieWorkFlowBundle\Dto\EditWorkflowTransitionRequestDto;
use CieWorkFlowBundle\Entity\WorkflowTransition;
use CieWorkFlowBundle\Model\Dto\RequestDtoInterface;
use CieWorkFlowBundle\Model\Service\DataCommandInterface;
use CieWorkFlowBundle\Repository\WorkflowTransitionRepository;
use CieWorkFlowBundle\Validator\ValidatorService\ValidationSrv;
use Doctrine\ORM\EntityNotFoundException;
use InvalidArgumentException;

class EditTransitionCommand implements DataCommandInterface
{
    private WorkflowTransitionRepository $repository;
    private WorkflowTransitionFactory $updater;
    private ValidationSrv $validationSrv;

    public function __construct(
        WorkflowTransitionRepository $repository, WorkflowTransitionFactory $updater, ValidationSrv $validationSrv
    )
    {
        $this->repository = $repository;
        $this->updater = $updater;
        $this->validationSrv = $validationSrv;
    }

    public function execute(RequestDtoInterface $requestDto)
    {
        if (! $requestDto instanceof EditWorkflowTransitionRequestDto) {
            throw new InvalidArgumentException(sprintf("Only %s is valid!", EditWorkflowTransitionRequestDto::class));
        }
        $this->validationSrv->validateAndThrow($requestDto);

        /** @var WorkflowTransition $workflowTransition */
        $workflowTransition = $this->repository->getObjectById($requestDto->id);

        if (! $workflowTransition) {
            throw new EntityNotFoundException(sprintf("Entity workflowTransition#%s not exists!", $requestDto->id));
        }

        $workflowTransition = $this->updater->fromRequest($requestDto, $workflowTransition);
        return $this->repository->save($workflowTransition);
    }
}