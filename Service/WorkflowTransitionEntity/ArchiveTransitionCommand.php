<?php

namespace CieWorkFlowBundle\Service\WorkflowTransitionEntity;

use CieWorkFlowBundle\Dto\ArchiveWorkflowTransitionRequestDto;
use CieWorkFlowBundle\Entity\WorkflowTransition;
use CieWorkFlowBundle\Model\Dto\RequestDtoInterface;
use CieWorkFlowBundle\Model\Service\DataCommandInterface;
use CieWorkFlowBundle\Repository\WorkflowTransitionRepository;
use CieWorkFlowBundle\Validator\ValidatorService\ValidationSrv;
use Doctrine\ORM\EntityNotFoundException;
use InvalidArgumentException;

class ArchiveTransitionCommand implements DataCommandInterface
{
    private WorkflowTransitionRepository $repository;
    private ValidationSrv $validationSrv;

    public function __construct(WorkflowTransitionRepository $repository, ValidationSrv $validationSrv)
    {
        $this->repository = $repository;
        $this->validationSrv = $validationSrv;
    }

    public function execute(RequestDtoInterface $requestDto)
    {
        if (! $requestDto instanceof ArchiveWorkflowTransitionRequestDto) {
            throw new InvalidArgumentException(sprintf("Only %s is valid!", ArchiveWorkflowTransitionRequestDto::class));
        }
        $this->validationSrv->validateAndThrow($requestDto);

        /** @var WorkflowTransition $workflowTransition */
        $workflowTransition = $this->repository->getObjectById($requestDto->id);

        if (! $workflowTransition) {
            throw new EntityNotFoundException(sprintf("Entity workflowTransition#%s not exists!", $requestDto->id));
        }

        return $this->repository->archive($workflowTransition);
    }
}