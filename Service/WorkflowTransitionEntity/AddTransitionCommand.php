<?php

namespace CieWorkFlowBundle\Service\WorkflowTransitionEntity;

use CieWorkFlowBundle\Dto\AddWorkflowTransitionRequestDto;
use CieWorkFlowBundle\Model\Dto\RequestDtoInterface;
use CieWorkFlowBundle\Model\Service\DataCommandInterface;
use CieWorkFlowBundle\Repository\WorkflowTransitionRepository;
use CieWorkFlowBundle\Validator\ValidatorService\ValidationSrv;
use InvalidArgumentException;

class AddTransitionCommand implements DataCommandInterface
{
    private WorkflowTransitionRepository $repository;
    private WorkflowTransitionFactory $factory;
    private ValidationSrv $validationSrv;

    public function __construct(
        WorkflowTransitionRepository $repository,
        WorkflowTransitionFactory $factory,
        ValidationSrv $validationSrv
    )
    {
        $this->repository = $repository;
        $this->factory = $factory;
        $this->validationSrv = $validationSrv;
    }

    public function execute(RequestDtoInterface $requestDto)
    {
        if (! $requestDto instanceof AddWorkflowTransitionRequestDto) {
            throw new InvalidArgumentException(sprintf("Only %s is valid!", AddWorkflowTransitionRequestDto::class));
        }
        $this->validationSrv->validateAndThrow($requestDto);

        // TODO: Add validators to unique name etc.
        $workflow = $this->factory->fromRequest($requestDto);
        return $this->repository->save($workflow);
    }
}