<?php

namespace CieWorkFlowBundle\Service\WorkflowTransitionEntity;

use CieWorkFlowBundle\Entity\WorkflowTransition;
use CieWorkFlowBundle\Model\Workflow\WorkflowTransitionInterface;
use CieWorkFlowBundle\Repository\WorkflowTransitionRepository;
use CieWorkFlowBundle\Service\SerializerTool;

class WorkflowTransitionQuerySrv
{
    private WorkflowTransitionRepository $workflowTransitionRepository;

    public function __construct(WorkflowTransitionRepository $workflowTransitionRepository)
    {
        $this->workflowTransitionRepository = $workflowTransitionRepository;
    }

    public function getAll(bool $serialized = false)
    {
        $steps = $this->workflowTransitionRepository->findAllUnarchived();

        if ($serialized) {
            $serializer = new SerializerTool();
            return $serializer->toArray($steps);
        }

        return $steps;
    }

    public function getById($id, bool $serialized = false)
    {
        $step = $this->workflowTransitionRepository->getObjectById($id);

        if ($serialized) {
            $serializer = new SerializerTool();
            return $serializer->toArray($step);
        }

        return $step;
    }

    public function getByName(string $name): ?WorkflowTransitionInterface
    {
        return $this->workflowTransitionRepository->getObjectByName($name);
    }
}