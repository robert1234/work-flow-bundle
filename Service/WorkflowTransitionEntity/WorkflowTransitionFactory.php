<?php

namespace CieWorkFlowBundle\Service\WorkflowTransitionEntity;

use CieWorkFlowBundle\Dto\AddWorkflowTransitionRequestDto;
use CieWorkFlowBundle\Dto\EditWorkflowTransitionRequestDto;
use CieWorkFlowBundle\Entity\WorkflowTransition;
use CieWorkFlowBundle\Model\Workflow\WorkflowTransitionInterface;
use CieWorkFlowBundle\Repository\PlaceRepository;
use CieWorkFlowBundle\Repository\WorkflowRepository;

class WorkflowTransitionFactory
{
    private WorkflowRepository $workflowRepository;
    private PlaceRepository $stepRepository;

    public function __construct(WorkflowRepository $workflowRepository, PlaceRepository $stepRepository)
    {
        $this->workflowRepository = $workflowRepository;
        $this->stepRepository = $stepRepository;
    }

    /**
     * @param AddWorkflowTransitionRequestDto|EditWorkflowTransitionRequestDto $requestDto
     * @param WorkflowTransitionInterface|null $workflowTransition
     * @return WorkflowTransition|WorkflowTransitionInterface|null
     */
    public function fromRequest($requestDto, WorkflowTransitionInterface $workflowTransition = null)
    {
        $fromStepEntity = $this->stepRepository->getObjectById($requestDto->from);
        $toStepEntity = $this->stepRepository->getObjectById($requestDto->to);
        $workflowEntity = $this->workflowRepository->getObjectById($requestDto->workflow);

        if (! $workflowTransition) {
            $workflowTransition = new WorkflowTransition();
        }

        $workflowTransition->setName($requestDto->name);
        $workflowTransition->setFrom($fromStepEntity);
        $workflowTransition->setTo($toStepEntity);
        $workflowTransition->setWorkflow($workflowEntity);
        $workflowTransition->setGroup($requestDto->group);
        $workflowTransition->setAdditionalAttributes($requestDto->additionalAttributes);
        $workflowTransition->setCheckerClass($requestDto->checkerClass);
        $workflowTransition->setLabel($requestDto->label);

        return $workflowTransition;
    }
}