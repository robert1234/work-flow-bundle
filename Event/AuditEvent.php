<?php

namespace CieWorkFlowBundle\Event;

use CieWorkFlowBundle\Model\Workflow\WorkflowObjectInterface;
use CieWorkFlowBundle\Model\Workflow\WorkflowTransitionInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class AuditEvent
{
    public WorkflowObjectInterface $workflowObject;
    public WorkflowTransitionInterface $transition;
    public UserInterface $user;

    public function getWorkflowObject(): WorkflowObjectInterface
    {
        return $this->workflowObject;
    }

    public function setWorkflowObject(WorkflowObjectInterface $workflowObject): self
    {
        $this->workflowObject = $workflowObject;
        return $this;
    }

    public function getTransition(): WorkflowTransitionInterface
    {
        return $this->transition;
    }

    public function setTransition(WorkflowTransitionInterface $transition): self
    {
        $this->transition = $transition;
        return $this;
    }

    public function getUser(): UserInterface
    {
        return $this->user;
    }

    public function setUser(UserInterface $user): self
    {
        $this->user = $user;
        return $this;
    }

    public static function create(WorkflowObjectInterface $workflowObject, WorkflowTransitionInterface $transition, UserInterface $user): self
    {
        return (new self())->setWorkflowObject($workflowObject)->setTransition($transition)->setUser($user);
    }
}