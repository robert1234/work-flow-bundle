<?php

namespace CieWorkFlowBundle\Validator\ValidatorService;

use CieWorkFlowBundle\Service\PlaceEntity\PlaceQuerySrv;
use CieWorkFlowBundle\Validator\Constraint\UniquePlace;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class UniquePlaceValidator extends ConstraintValidator
{
    private PlaceQuerySrv $stepQuerySrv;

    public function __construct(PlaceQuerySrv $stepQuerySrv)
    {
        $this->stepQuerySrv = $stepQuerySrv;
    }

    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof UniquePlace) {
            throw new UnexpectedTypeException($constraint, UniquePlace::class);
        }

        if (null === $value || '' === $value) {
            return;
        }

        $existed = $this->stepQuerySrv->getByName($value);
        $request = $this->context->getObject();

        $result = $this->isSame($existed, $request);

        if (! $existed || $result) {
            return;
        }

        $this->context->buildViolation($constraint->message)->setParameter('{{name}}', $value)->addViolation();
    }

    private function isSame($existed, $requested): bool
    {
        if (method_exists($requested, 'getId')) {
            return ($existed) && ($existed->getId() == $requested->getId());
        }

        return false;
    }
}