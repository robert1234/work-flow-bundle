<?php

namespace CieWorkFlowBundle\Validator\ValidatorService;

use CieWorkFlowBundle\Service\WorkflowEntity\WorkflowQuerySrv;
use CieWorkFlowBundle\Validator\Constraint\UniqueWorkflow;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class UniqueWorkflowValidator extends ConstraintValidator
{
    private WorkflowQuerySrv $querySrv;

    public function __construct(WorkflowQuerySrv $querySrv)
    {
        $this->querySrv = $querySrv;
    }

    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof UniqueWorkflow) {
            throw new UnexpectedTypeException($constraint, UniqueWorkflow::class);
        }

        if (null === $value || '' === $value) {
            return;
        }

        $existed = $this->querySrv->getByName($value);
        $request = $this->context->getObject();

        $result = $this->isSame($existed, $request);

        if (! $existed || $result) {
            return;
        }

        $this->context->buildViolation($constraint->message)->setParameter('{{name}}', $value)->addViolation();
    }

    private function isSame($existed, $requested): bool
    {
        if (method_exists($requested, 'getId')) {
            return ($existed) && ($existed->getId() == $requested->getId());
        }

        return false;
    }
}