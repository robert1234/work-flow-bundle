<?php

namespace CieWorkFlowBundle\Validator\ValidatorService;

use CieWorkFlowBundle\Service\WorkflowTransitionEntity\WorkflowTransitionQuerySrv;
use CieWorkFlowBundle\Validator\Constraint\UniqueTransition;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class UniqueTransitionValidator extends ConstraintValidator
{
    private WorkflowTransitionQuerySrv $workflowQuerySrv;

    public function __construct(WorkflowTransitionQuerySrv $querySrv)
    {
        $this->workflowQuerySrv = $querySrv;
    }

    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof UniqueTransition) {
            throw new UnexpectedTypeException($constraint, UniqueTransition::class);
        }

        if (null === $value || '' === $value) {
            return;
        }

        $existed = $this->workflowQuerySrv->getByName($value);
        $request = $this->context->getObject();

        $result = $this->isSame($existed, $request);

        if (! $existed || $result) {
            return;
        }

        $this->context->buildViolation($constraint->message)->setParameter('{{name}}', $value)->addViolation();
    }

    private function isSame($existed, $requested): bool
    {
        if (method_exists($requested, 'getId')) {
            return ($existed) && ($existed->getId() == $requested->getId());
        }

        return false;
    }
}