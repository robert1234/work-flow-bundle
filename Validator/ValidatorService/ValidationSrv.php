<?php

namespace CieWorkFlowBundle\Validator\ValidatorService;

use Exception;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ValidationSrv
{
    private ValidatorInterface $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function validate($object): ?string
    {
        $result = null;

        $errors = $this->validator->validate($object);

        if ($errors->count() > 0) {
            $result = (string) $errors;
        }

        return $result;
    }

    public function validateAndThrow($object): ?string
    {
        $validationResult = $this->validate($object);
        if ($validationResult) {
            throw new Exception($validationResult);
        }

        return $validationResult;
    }
}