<?php

namespace CieWorkFlowBundle\Validator\Constraint;

use CieWorkFlowBundle\Validator\ValidatorService\UniqueWorkflowValidator;
use Symfony\Component\Validator\Constraint;

/** @Annotation */
class UniqueWorkflow extends Constraint
{
    public string $message = 'The workflow named {{name}} exists!';

    public function validatedBy()
    {
        return UniqueWorkflowValidator::class;
    }
}