<?php

namespace CieWorkFlowBundle\Validator\Constraint;

use CieWorkFlowBundle\Validator\ValidatorService\UniquePlaceValidator;
use Symfony\Component\Validator\Constraint;

/** @Annotation */
class UniquePlace extends Constraint
{
    public string $message = 'The step named {{name}} exists!';

    public function validatedBy()
    {
        return UniquePlaceValidator::class;
    }
}