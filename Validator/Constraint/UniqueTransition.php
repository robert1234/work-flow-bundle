<?php

namespace CieWorkFlowBundle\Validator\Constraint;

use CieWorkFlowBundle\Validator\ValidatorService\UniqueTransitionValidator;
use Symfony\Component\Validator\Constraint;

/** @Annotation */
class UniqueTransition extends Constraint
{
    public string $message = 'The workflow transition named {{name}} exists!';

    public function validatedBy()
    {
        return UniqueTransitionValidator::class;
    }
}