<?php

namespace CieWorkFlowBundle\Repository;

use CieWorkFlowBundle\Entity\Workflow;
use CieWorkFlowBundle\Model\Repository\AbstractRepository;
use CieWorkFlowBundle\Model\Workflow\WorkflowInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method WorkflowInterface|null find($id, $lockMode = null, $lockVersion = null)
 * @method WorkflowInterface|null findOneBy(array $criteria, array $orderBy = null)
 * @method WorkflowInterface[]    findAll()
 * @method WorkflowInterface[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WorkflowRepository extends AbstractRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Workflow::class);
    }

    public function getObjectById($id)
    {
        return $this->find($id);
    }

    public function getObjectByName($name)
    {
        return $this->findOneBy(['name' => $name]);
    }

    public function findAllUnarchived()
    {
        return $this->findBy(['archivedAt' => null]);
    }

    public function findBySupportedClass(string $className)
    {
        return $this->findBy(['archivedAt' => null, 'supportedClass' => $className]);
    }
}