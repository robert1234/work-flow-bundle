<?php

namespace CieWorkFlowBundle\Repository;

use CieWorkFlowBundle\Entity\WorkflowTransition;
use CieWorkFlowBundle\Model\Repository\AbstractRepository;
use CieWorkFlowBundle\Model\Place\PlaceInterface;
use CieWorkFlowBundle\Model\Workflow\WorkflowInterface;
use CieWorkFlowBundle\Model\Workflow\WorkflowTransitionInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Workflow\Transition;

/**
 * @method WorkflowTransitionInterface|null find($id, $lockMode = null, $lockVersion = null)
 * @method WorkflowTransitionInterface|null findOneBy(array $criteria, array $orderBy = null)
 * @method WorkflowTransitionInterface[]    findAll()
 * @method WorkflowTransitionInterface[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WorkflowTransitionRepository extends AbstractRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WorkflowTransition::class);
    }

    public function getObjectById($id)
    {
        return $this->find($id);
    }

    public function getObjectByName($name)
    {
        return $this->findOneBy(['name' => $name]);
    }

    public function findAllUnarchived()
    {
        return $this->findBy(['archivedAt' => null]);
    }

    public function findByWorkflow(WorkflowInterface $workflow)
    {
        $query = $this->getEntityManager()->getRepository(WorkflowTransition::class)
            ->createQueryBuilder('t')
            ->select()
            ->leftJoin('t.workflow', 'w')
            ->where('w.name = :name')
            ->andWhere('t.archivedAt IS NULL')
            ->setParameter("name", $workflow->getName());

        return $query->getQuery()->getResult();
    }

    public function findByWorkflowAndFromStep(WorkflowInterface $workflow, PlaceInterface $step)
    {
        $query = $this->getEntityManager()->getRepository(WorkflowTransition::class)
            ->createQueryBuilder('t')
            ->select()
            ->leftJoin('t.workflow', 'w')
            ->leftJoin('t.from', 'f')
            ->where('w.name = :name')
            ->andWhere('t.archivedAt IS NULL')
            ->andWhere('f.name = :from')
            ->setParameter("name", $workflow->getName())
            ->setParameter("from", $step->getId());

        return $query->getQuery()->getResult();
    }

    public function getObjectsBySymfonyTransitions(array $symfonyTransitions): Collection
    {
        $result = new ArrayCollection();
        /** @var Transition $transition */
        foreach ($symfonyTransitions as $transition) {
            $result->add($this->getObjectByName($transition->getName()));
        }

        return $result;
    }
}