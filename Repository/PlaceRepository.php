<?php

namespace CieWorkFlowBundle\Repository;

use CieWorkFlowBundle\Entity\Place;
use CieWorkFlowBundle\Model\Repository\AbstractRepository;
use CieWorkFlowBundle\Model\Place\PlaceInterface;
use CieWorkFlowBundle\Model\Workflow\WorkflowInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PlaceInterface|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlaceInterface|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlaceInterface[]    findAll()
 * @method PlaceInterface[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlaceRepository extends AbstractRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Place::class);
    }

    public function getObjectById($id)
    {
        return $this->find($id);
    }

    public function getObjectByName($name)
    {
        return $this->findOneBy(['name' => $name]);
    }

    public function findAllUnarchived()
    {
        return $this->findBy(['archivedAt' => null]);
    }

    public function findByWorkflow(WorkflowInterface $workflow)
    {
        $query = $this->getEntityManager()->getRepository(Place::class)
            ->createQueryBuilder('s')
            ->select()
            ->leftJoin('s.transitionsAtFrom', 'tf')
            ->leftJoin('tf.workflow', 'wf')
            ->leftJoin('s.transitionsAtTo', 'tt')
            ->leftJoin('tt.workflow', 'wt')
            ->where('wf.name = :name OR wt.name = :name')
            ->andWhere('s.archivedAt IS NULL')
            ->setParameter("name", $workflow->getName());

        return $query->getQuery()->getResult();
    }

    public function findNamesByWorkflowAtFrom(WorkflowInterface $workflow)
    {
        $query = $this->getEntityManager()->getRepository(Place::class)
            ->createQueryBuilder('s')
            ->select('s.name')
            ->leftJoin('s.transitionsAtFrom', 'tf')
            ->leftJoin('tf.workflow', 'wf')
            ->where('wf.name = :name')
            ->andWhere('s.archivedAt IS NULL')
            ->setParameter("name", $workflow->getName());

        return $query->getQuery()->getResult();
    }

    public function findNamesByWorkflowAtTo(WorkflowInterface $workflow)
    {
        $query = $this->getEntityManager()->getRepository(Place::class)
            ->createQueryBuilder('s')
            ->select('s.name')
            ->leftJoin('s.transitionsAtTo', 'tt')
            ->leftJoin('tt.workflow', 'wt')
            ->where('wt.name = :name')
            ->andWhere('s.archivedAt IS NULL')
            ->setParameter("name", $workflow->getName());

        return $query->getQuery()->getResult();
    }
}