<?php

namespace CieWorkFlowBundle\EventDispatcher;

use CieWorkFlowBundle\Model\EventDispatcher\EventBusInterface;
use SimpleBus\SymfonyBridge\Bus\EventBus;

class WorkflowEventBus implements EventBusInterface
{
    private EventBus $eventBus;

    public function __construct(EventBus $eventBus)
    {
        $this->eventBus = $eventBus;
    }

    public function handle($message)
    {
        $this->eventBus->handle($message);
    }
}