<?php

namespace CieWorkFlowBundle\Dto;

use CieWorkFlowBundle\Validator\Constraint\UniqueWorkflow;
use Symfony\Component\Validator\Constraints as Assert;

abstract class WorkflowRequestDto
{
    /**
     * @UniqueWorkflow()
     * @Assert\NotBlank()
     */
    public string $name;
    public ?string $description = null;
    public string $supportedClass;
    public array $additionalAttributes = [];
}