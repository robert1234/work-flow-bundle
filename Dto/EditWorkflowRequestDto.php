<?php

namespace CieWorkFlowBundle\Dto;

use CieWorkFlowBundle\Model\Dto\RequestDtoInterface;

class EditWorkflowRequestDto extends WorkflowRequestDto implements RequestDtoInterface
{
    public int $id;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public static function fromArray(array $data): self
    {
        $result = new self();
        $result->id = $data['id'];
        $result->name = $data['name'];
        $result->supportedClass = $data['supported_class'];

        if (isset($data['description'])) {
            $result->description = $data['description'];
        }

        if (isset($data['additional_attributes'])) {
            $result->additionalAttributes = $data['additional_attributes'];
        }

        return $result;
    }
}