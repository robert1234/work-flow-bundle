<?php

namespace CieWorkFlowBundle\Dto;

use CieWorkFlowBundle\Model\Dto\RequestDtoInterface;

class AddPlaceRequestDto extends PlaceRequestDto implements RequestDtoInterface
{
    public static function fromArray(array $data): self
    {
        $request = new self();
        $request->name = $data['name'];
        $request->actionClass = $data['action_class'];

        if (isset($data['additional_attributes'])) {
            $request->additionalAttributes = $data['additional_attributes'];
        }

        if (isset($data['label'])) {
            $request->label = $data['label'];
        }

        return $request;
    }
}