<?php

namespace CieWorkFlowBundle\Dto;

use CieWorkFlowBundle\Model\Dto\RequestDtoInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class AddWorkflowWithTransitionsRequestDto implements RequestDtoInterface
{
    public AddWorkflowRequestDto $workflowRequestDto;
    public Collection $workflowTransitionRequestDtos;

    public function __construct()
    {
        $this->workflowTransitionRequestDtos = new ArrayCollection();
    }

    public static function fromArray(array $data): RequestDtoInterface
    {
        $request = new self();

        $workflowRequest = AddWorkflowRequestDto::fromArray($data['workflow']);
        $request->workflowRequestDto = $workflowRequest;

        foreach ($data['transitions'] as $transition) {
            $transitionRequest = AddWorkflowTransitionRequestDto::fromArray($transition);
            $request->workflowTransitionRequestDtos->add($transitionRequest);
        }

        return $request;
    }
}