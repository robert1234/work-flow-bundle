<?php

namespace CieWorkFlowBundle\Dto;

use CieWorkFlowBundle\Model\Dto\RequestDtoInterface;

class AddWorkflowTransitionRequestDto extends WorkflowTransitionRequestDto implements RequestDtoInterface
{
    public static function fromArray(array $data): self
    {
        $result = new self();
        $result->name = $data['name'];
        $result->workflow = $data['workflow'];
        $result->to = $data['to'];
        $result->from = $data['from'];
        $result->checkerClass = $data['checker_class'];

        if (isset($data['label'])) {
            $result->label = $data['label'];
        }

        if (isset($data['group'])) {
            $result->group = $data['group'];
        }

        if (isset($data['additional_attributes'])) {
            $result->additionalAttributes = $data['additional_attributes'];
        }

        return $result;
    }
}