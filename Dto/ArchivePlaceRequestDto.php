<?php

namespace CieWorkFlowBundle\Dto;

use CieWorkFlowBundle\Model\Dto\RequestDtoInterface;

class ArchivePlaceRequestDto implements RequestDtoInterface
{
    public ?int $id;
    public ?string $name;

    public static function fromArray(array $data): self
    {
        $result = new self();

        if (isset($data['id'])) {
            $result->id = $data['id'];
        }

        if (isset($data['name'])) {
            $result->name = $data['name'];
        }

        return $result;
    }
}