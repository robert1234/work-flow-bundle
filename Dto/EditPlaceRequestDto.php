<?php

namespace CieWorkFlowBundle\Dto;

use CieWorkFlowBundle\Model\Dto\RequestDtoInterface;

class EditPlaceRequestDto extends PlaceRequestDto implements RequestDtoInterface
{
    public int $id;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public static function fromArray(array $data): self
    {
        $request = new self();
        $request->id = $data['id'];
        $request->name = $data['name'];
        $request->actionClass = $data['action_class'];

        if (isset($data['label'])) {
            $request->label = $data['label'];
        }

        if (isset($data['additional_attributes'])) {
            $request->additionalAttributes = $data['additional_attributes'];
        }

        return $request;
    }
}