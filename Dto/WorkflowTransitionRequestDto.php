<?php

namespace CieWorkFlowBundle\Dto;

use CieWorkFlowBundle\Validator\Constraint\UniqueTransition;
use Symfony\Component\Validator\Constraints as Assert;

class WorkflowTransitionRequestDto
{
    /**
     * @UniqueTransition
     * @Assert\NotBlank()
     */
    public string $name;
    public ?string $label = null;
    public ?string $group = null;
    public array $additionalAttributes = [];
    public int $from;
    public int $to;
    public int $workflow;
    public string $checkerClass;
}