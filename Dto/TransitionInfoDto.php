<?php

namespace CieWorkFlowBundle\Dto;

use CieWorkFlowBundle\Model\Workflow\WorkflowTransitionInterface;
use Symfony\Component\Workflow\Transition;

class TransitionInfoDto
{
    protected string $name;
    protected string $fromName;
    protected string $toName;

    public function getName(): string
    {
        return $this->name;
    }
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }
    public function getFromName(): string
    {
        return $this->fromName;
    }
    public function setFromName(string $fromName): self
    {
        $this->fromName = $fromName;
        return $this;
    }
    public function getToName(): string
    {
        return $this->toName;
    }
    public function setToName(string $toName): self
    {
        $this->toName = $toName;
        return $this;
    }

    public static function create(WorkflowTransitionInterface $transition)
    {
        return (new self())
            ->setName($transition->getName())
            ->setFromName($transition->getFrom()->getName())
            ->setToName($transition->getTo()->getName());
    }

    public function toSymfonyTransition()
    {
        return new Transition($this->getName(), $this->getFromName(), $this->getToName());
    }
}