<?php

namespace CieWorkFlowBundle\Dto;

use CieWorkFlowBundle\Model\Dto\RequestDtoInterface;

class EditWorkflowTransitionRequestDto extends WorkflowTransitionRequestDto implements RequestDtoInterface
{
    public int $id;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public static function fromArray(array $data): self
    {
        $result = new self();
        $result->id = $data['id'];
        $result->name = $data['name'];
        $result->workflow = $data['workflow'];
        $result->to = $data['to'];
        $result->from = $data['from'];
        $result->checkerClass = $data['checker_class'];

        if (isset($data['label'])) {
            $result->label = $data['label'];
        }

        if (isset($data['group'])) {
            $result->group = $data['group'];
        }

        if (isset($data['additional_attributes'])) {
            $result->additionalAttributes = $data['additional_attributes'];
        }

        return $result;
    }
}