<?php

namespace CieWorkFlowBundle\Dto;

use CieWorkFlowBundle\Validator\Constraint\UniquePlace;
use Symfony\Component\Validator\Constraints as Assert;

class PlaceRequestDto
{
    /**
     * @UniquePlace()
     * @Assert\NotBlank()
     */
    public string $name;
    public ?string $label = null;
    public string $actionClass;
    public array $additionalAttributes = [];
}