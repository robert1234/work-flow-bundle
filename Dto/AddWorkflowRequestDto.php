<?php

namespace CieWorkFlowBundle\Dto;

use CieWorkFlowBundle\Model\Dto\RequestDtoInterface;

class AddWorkflowRequestDto extends WorkflowRequestDto implements RequestDtoInterface
{
    public static function fromArray(array $data): self
    {
        $result = new self();
        $result->name = $data['name'];
        $result->supportedClass = $data['supported_class'];

        if (isset($data['description'])) {
            $result->description = $data['description'];
        }

        if (isset($data['additional_attributes'])) {
            $result->additionalAttributes = $data['additional_attributes'];
        }

        return $result;
    }
}