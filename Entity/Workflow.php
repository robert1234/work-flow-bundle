<?php

namespace CieWorkFlowBundle\Entity;

use CieWorkFlowBundle\Dto\AddWorkflowRequestDto;
use CieWorkFlowBundle\Model\Dto\RequestDtoInterface;
use CieWorkFlowBundle\Model\Repository\SoftRemovableTrait;
use CieWorkFlowBundle\Model\Workflow\WorkflowInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="CieWorkFlowBundle\Repository\WorkflowRepository")
 * @ORM\Table(name="workflow_bundle_workflow")
 */
class Workflow implements WorkflowInterface
{
    use SoftRemovableTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     */
    protected int $id;

    /**
     * @ORM\Column(name="name", type="string", length=64)
     */
    protected string $name;

    /**
     * @ORM\Column(name="description", type="string", length=1024, nullable=true)
     */
    protected ?string $description;

    /**
     * @ORM\Column(name="supported_class", type="string", length=256)
     */
    protected string $supportedClass;

    /**
     * @ORM\Column(name="additional_attributes", type="json")
     */
    protected array $additionalAttributes = array();

    /**
     * @ORM\OneToMany(targetEntity="CieWorkFlowBundle\Entity\WorkflowTransition", mappedBy="workflow")
     */
    protected Collection $transitions;

    public function __construct()
    {
        $this->transitions = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getSupportedClass(): string
    {
        return $this->supportedClass;
    }

    public function setSupportedClass(string $supportedClass): self
    {
        $this->supportedClass = $supportedClass;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;
        return $this;
    }

    public function getAdditionalAttributes(): array
    {
        return $this->additionalAttributes;
    }

    public function getAdditionalAttribute($name)
    {
        return isset($this->additionalAttributes[$name]) ? $this->additionalAttributes[$name] : null;
    }

    public function setAdditionalAttributes(array $additionalAttributes): self
    {
        $this->additionalAttributes = $additionalAttributes;
        return $this;
    }

    public function getTransitions(): Collection
    {
        return $this->transitions;
    }

    public function setTransitions(Collection $transitions): self
    {
        $this->transitions = $transitions;
        return $this;
    }

    public function isClassSupported($className): bool
    {
        return $this->supportedClass == $className;
    }

    public static function create(AddWorkflowRequestDto $requestDto)
    {
        $workflow = new Workflow();
        $workflow->setName($requestDto->name);
        $workflow->setDescription($requestDto->description);
        $workflow->setSupportedClass($requestDto->supportedClass);
        $workflow->setAdditionalAttributes($requestDto->additionalAttributes);

        return $workflow;
    }

    public function toArray(): array
    {
        $data = [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'supported_class' => $this->supportedClass,
            'additional_attributes' => $this->additionalAttributes,
            'archived_at' => ($this->archivedAt) ? $this->archivedAt->format("Y-m-d H:i:s") : null,
            'transitions' => []
        ];

        /** @var WorkflowTransition $transition */
        foreach ($this->transitions as $transition) {
            if ($transition->isValid()) {
                $data['transitions'][] = $transition->toArray();
            }
        }

        return $data;
    }
}