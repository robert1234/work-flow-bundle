<?php

namespace CieWorkFlowBundle\Entity;

use CieWorkFlowBundle\Dto\AddWorkflowTransitionRequestDto;
use CieWorkFlowBundle\Model\Repository\SoftRemovableTrait;
use CieWorkFlowBundle\Model\Place\PlaceInterface;
use CieWorkFlowBundle\Model\Workflow\WorkflowInterface;
use CieWorkFlowBundle\Model\Workflow\WorkflowTransitionInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="CieWorkFlowBundle\Repository\WorkflowTransitionRepository")
 * @ORM\Table(name="workflow_bundle_transition")
 */
class WorkflowTransition implements WorkflowTransitionInterface
{
    use SoftRemovableTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     */
    protected int $id;

    /**
     * @ORM\Column(name="name", type="string", length=255)
     */
    protected string $name;

    /**
     * @ORM\Column(name="label", type="string", length=64, nullable=true)
     */
    protected ?string $label;

    /**
     * @ORM\Column(name="group_name", type="string", length=255, nullable=true)
     */
    protected ?string $group;

    /**
     * @ORM\Column(name="additional_attributes", type="json")
     */
    protected array $additionalAttributes = array();

    /**
     * @ORM\ManyToOne(targetEntity="Place", inversedBy="transitionsAtFrom")
     * @ORM\JoinColumn(nullable=false, name="place_from_id")
     */
    protected PlaceInterface $from;

    /**
     * @ORM\ManyToOne(targetEntity="Place", inversedBy="transitionsAtTo")
     * @ORM\JoinColumn(nullable=false, name="place_to_id")
     */
    protected PlaceInterface $to;

    /**
     * @ORM\ManyToOne(targetEntity="CieWorkFlowBundle\Entity\Workflow", inversedBy="transitions")
     * @ORM\JoinColumn(nullable=false, name="workflow_id")
     */
    protected WorkflowInterface $workflow;

    /**
     * @ORM\Column(name="checker_class", type="string", length=256, nullable=true)
     */
    protected ?string $checkerClass = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getGroup(): ?string
    {
        return $this->group;
    }

    public function setGroup(?string $group): self
    {
        $this->group = $group;
        return $this;
    }

    public function getAdditionalAttributes(): array
    {
        return $this->additionalAttributes;
    }

    public function setAdditionalAttributes(array $additionalAttributes): self
    {
        $this->additionalAttributes = $additionalAttributes;
        return $this;
    }

    public function getFrom(): PlaceInterface
    {
        return $this->from;
    }

    public function setFrom(PlaceInterface $from): self
    {
        $this->from = $from;
        return $this;
    }

    public function getTo(): PlaceInterface
    {
        return $this->to;
    }

    public function setTo(PlaceInterface $to): self
    {
        $this->to = $to;
        return $this;
    }

    public function getWorkflow(): WorkflowInterface
    {
        return $this->workflow;
    }

    public function setWorkflow(WorkflowInterface $workflow): self
    {
        $this->workflow = $workflow;
        return $this;
    }

    public function getCheckerClass(): string
    {
        return $this->checkerClass;
    }

    public function setCheckerClass(string $checkerClass): self
    {
        $this->checkerClass = $checkerClass;
        return $this;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(?string $label): self
    {
        $this->label = $label;
        return $this;
    }

    public function isValid(): bool
    {
        return $this->archivedAt == null;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'group' => $this->group,
            'additional_attributes' => $this->additionalAttributes,
            'checker_class' => $this->checkerClass,
            'from' => $this->from->getId(),
            'to' => $this->to->getId(),
            'archived_at' => ($this->archivedAt) ? $this->archivedAt->format("Y-m-d H:i:s") : null,
            'workflow' => ($this->workflow) ? $this->workflow->getId() : null,
            'label' => $this->label
        ];
    }
}