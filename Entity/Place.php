<?php

namespace CieWorkFlowBundle\Entity;

use CieWorkFlowBundle\Dto\AddPlaceRequestDto;
use CieWorkFlowBundle\Model\Repository\SoftRemovableTrait;
use CieWorkFlowBundle\Model\Place\PlaceInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="CieWorkFlowBundle\Repository\PlaceRepository")
 * @ORM\Table(name="workflow_bundle_place")
 */
class Place implements PlaceInterface
{
    use SoftRemovableTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     */
    protected int $id;

    /**
     * @ORM\Column(name="name", type="string", length=64)
     */
    protected string $name;

    /**
     * @ORM\Column(name="label", type="string", length=64, nullable=true)
     */
    protected ?string $label;

    /**
     * @ORM\Column(name="action_class", type="string", length=256)
     */
    protected string $actionClass;

    /**
     * @ORM\Column(name="additional_attributes", type="json")
     */
    protected array $additionalAttributes = array();

    /**
     * @ORM\OneToMany(targetEntity="CieWorkFlowBundle\Entity\WorkflowTransition", mappedBy="from")
     */
    protected Collection $transitionsAtFrom;

    /**
     * @ORM\OneToMany(targetEntity="CieWorkFlowBundle\Entity\WorkflowTransition", mappedBy="to")
     */
    protected Collection $transitionsAtTo;

    public function __construct()
    {
        $this->transitionsAtFrom = new ArrayCollection();
        $this->transitionsAtTo = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getActionClass(): string
    {
        return $this->actionClass;
    }

    public function setActionClass(string $actionClass): self
    {
        $this->actionClass = $actionClass;
        return $this;
    }

    public function getAdditionalAttributes(): array
    {
        return $this->additionalAttributes;
    }

    public function getAdditionalAttribute($name)
    {
        return isset($this->additionalAttributes[$name]) ? $this->additionalAttributes[$name] : null;
    }

    public function setAdditionalAttributes(array $additionalAttributes): self
    {
        $this->additionalAttributes = $additionalAttributes;
        return $this;
    }

    public function getTransitionsAtFrom(): Collection
    {
        return $this->transitionsAtFrom;
    }

    public function setTransitionsAtFrom(Collection $transitionsAtFrom): self
    {
        $this->transitionsAtFrom = $transitionsAtFrom;
        return $this;
    }

    public function getTransitionsAtTo(): Collection
    {
        return $this->transitionsAtTo;
    }

    public function setTransitionsAtTo(Collection $transitionsAtTo): self
    {
        $this->transitionsAtTo = $transitionsAtTo;
        return $this;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(?string $label): self
    {
        $this->label = $label;
        return $this;
    }

    public static function create(AddPlaceRequestDto $requestDto)
    {
        $step = new Place();
        $step->setName($requestDto->name);
        $step->setActionClass($requestDto->actionClass);
        $step->setAdditionalAttributes($requestDto->additionalAttributes);
        $step->setLabel($requestDto->label);

        return $step;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'action_class' => $this->actionClass,
            'additional_attributes' => $this->additionalAttributes,
            'label' => $this->label
        ];
    }
}